package com.example.ejemplo01;

import java.util.List;

import org.springframework.data.repository.Repository;

public interface PersonaRepositorio extends Repository<Persona, Integer> {
	List<Persona>findAll();
	Persona save(Persona p);
	Persona findById(int id);
	void delete(Persona p);
}
